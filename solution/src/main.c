#include "image.h"
#include "bmp_io.h"
#include "image_rotate.h"

#include <assert.h>
#include <malloc.h>
#include <stdbool.h>

bool open_file(FILE **file, const char *filename, const char *mode) {
    if (!(*file = fopen(filename, mode))) {
        printf("Error! Can't open file %s\n", filename);
        return false;
    }
    return true;
}

bool close_file(FILE **file) {
    if (*file && fclose(*file) != 0) {
        printf("Error! Can't close file\n");
        return false;
    }
    *file = NULL;
    return true;
}

int main(int args, char **argv) {

    if (args != 3) {
        printf("Not enough arguments");
        return 1;
    }
    char const *file_input = argv[1];
    char const *file_output = argv[2];
    FILE* input = NULL;
    FILE* output = NULL;

    if (!open_file(&input, file_input, "rb")) {
        return 1;
    }
    if (!open_file(&output, file_output, "wb")) {
        close_file(&input);
        return 1;
    }

    struct image image = {0};
    if (from_bmp(input, &image) != READ_OK) {
        close_file(&input);
        close_file(&output);
        image_free(image);
        return 1;
    }
    struct image rotated_image = rotate(&image);
    if (to_bmp(output, &rotated_image) != WRITE_OK) {
        close_file(&input);
        close_file(&output);
        image_free(image);
        image_free(rotated_image);
        return 1;
    }
    if (!close_file(&input) || !close_file(&output)) {
        return 1;
    }
    image_free(image);
    image_free(rotated_image);

    return 0;
}
