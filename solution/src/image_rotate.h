#ifndef LAB3_IMAGE_ROTATE_H
#define LAB3_IMAGE_ROTATE_H
#include "image.h"

struct image rotate(struct image const* source);
void image_free(struct image img);
struct image image_create(size_t width, size_t height);
#endif //LAB3_IMAGE_ROTATE_H
